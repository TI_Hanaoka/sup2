import { Component } from '@angular/core';

@Component({
  selector: 'sup2-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sup2';
}
