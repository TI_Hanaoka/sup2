export const environment = {
  production: true,
  apiHost: 'https://sup-api-production.cfapps.io'
};
